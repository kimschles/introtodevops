# Cloud Infrastructure
### Objectives
By the end of this lab you will be able to:

- Reuse and change a Cloud Formation Template
- Describe a change in AWS infrastructure vs a change in an EC2 instance
- Relish in the beauty of auto-deployed infrastructure 

## Part 2: Tearing It Up

Recommended working directory: `~/gitlab/devopsclass/cloud_infrastructure`

---

One of the fundamental concepts in the agile methodology is an [iterative approach](https://en.wikipedia.org/wiki/Agile_software_development#Iterative_vs._Waterfall).  Avoiding [snowflakes](http://martinfowler.com/bliki/SnowflakeServer.html) is a critical aspect of the speed and agility we're trying to attain with DevOps.  Losing a server should never induce a panic.

---

### Tear Down Your Infrastructure
- Navigate to the CloudFormation console
- Select the stack you previously created
- Click the "Actions" button and select "Delete Stack"

---

### Make an Improvement
- Build out the UserData script in the `demo_stack.yml`
  - Update the server when it first boots
  - Install Docker
  - Add the ubuntu user to docker group
  - Reboot (in case any updates require it)

  ```yaml
  UserData:
    "Fn::Base64":
      "Fn::Join":
        - "\n"
        - - "#!/bin/bash"
          - apt update
          - apt install -y docker.io
          - usermod -aG docker ubuntu
          - reboot
  ```

---

### Rebuild the Stack
- Use CloudFormation to build your newly updated stack!

![image](aws2.png?)

---

### Check your changes
- Go to the AWS EC2 console and find the public IP of your instance
  - This should be the elastic IP that you created in the previous lab
- Log in to the server and verify the Docker install

  ```bash
  ssh -i ~/.ssh/demokey.pem ubuntu@<elastic_ip>
  docker --version
  ```
  NOTE - if you get the warning: "WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!" try this alternate version of SSH command:

  ```bash
  ssh -i ~/.ssh/demokey.pem -o StrictHostKeyChecking=no ubuntu@<elastic_ip>
  docker --version
  ```

---

### Things to Note
- It's really easy to tear it all down, make changes, and build it up again.

---

### Relevant Topics to Research on Your Own
- Configuration Management
- Immutable Infrastructure
- Agile Methodology

---

|Previous: [Cloud Infrastructure 1](/labs/04_cloud_infrastructure_1/cloud_infrastructure_1.md)|Next: [Continuous Integration](/labs/06_continuous_integration/continuous_integration.md)|
|----:|:----|
