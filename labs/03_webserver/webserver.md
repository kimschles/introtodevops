# A Really Basic WebServer (in Docker)

*By the end of this lab, you will be able to:*

 * build and run a local webserver container
 * select container images purposefully about image for your docker builds



For this lab, use the repository you created in the [GitLab lab](../01/gitlab.md).  
If you followed the instructions verbatim, it should be `~/gitlab/mywebserver`

```bash
cd ~/gitlab/mywebserver
```

---

### Make a Webpage

```bash
mkdir html
echo This is my first web page > html/index.html
open html/index.html
```

NOTE: if the open command gives you an error try:

```
xdg-open html/index.html`
```
---

### Make a Webserver with Docker

Copy our webserver Dockerfile to mywebserver folder

```
cp ~/gitlab/introtodevops/labs/03_webserver/files/Dockerfile .
```

Contents of Dockerfile:

```Dockerfile
FROM httpd:2.4

COPY ./html /usr/local/apache2/htdocs/
```

And build your Docker image:

```bash
docker build -t webserver .
```

![image](make_webserver.png?)


---

### Run and Test Your Webserver

Run your webserver with the `-P` option to expose the ports, and (optionally) give it a name with the `--name` flag:

```bash
docker run --detach -p80:80 --name mywebserver webserver
```

Now run `docker ps` to see information about your running container:

```
CONTAINER ID        IMAGE               COMMAND              CREATED             STATUS              PORTS                   NAMES
c937baecda34        webserver           "httpd-foreground"   3 seconds ago       Up 1 seconds        0.0.0.0:80->80/tcp   mywebserver
```

Finally, test your webserver to see if it's running:

```bash
open http://localhost
```

You should also be able to test http://localhost in your web browser.

---

|Previous: [Docker](/labs/02_docker/docker.md)|Next: [Cloud Infrastructure 1](/labs/04_cloud_infrastructure_1/cloud_infrastructure_1.md)|
|----:|:----|
