# Continuous Integration
*By the end of this lab, you will:*
1. Configure a gitlab pipeline 
1. Setup a stage to build, test and push the webserver
1. Define Continuous Integration
1. Explain what _pipeline_, _stage_, _jobs_ and _scripts_ mean in the context of Continuous Integration


Required working directory: `~/gitlab/mywebserver`  
(This is the root of your webserver repository)

---

Most continuous integration platforms start by cloning your repository.  You're then able to run build/test jobs against a clean environment with the latest copy of your (or your developer's) code.  You can accomplish something very similar by creating a new directory on your machine and cloning a new copy of your repo - which can be a really good way to test quickly and troubleshoot errors with your CI system.

---


GitLab has [Pipelines](https://docs.gitlab.com/ce/ci/pipelines.html) that perform continuous integration tasks for us.  

To use pipelines, we just need to create a `.gitlab-ci.yml` file.  There are [a lot of options](https://docs.gitlab.com/ce/ci/yaml/README.html) for this file, but we're just going to cover some basics.

---

### Terminology

*pipeline* - A collection of stages, jobs, and scripts that will be run when a new commit is checked in to the repository.
*stages* - Will be run sequentially.  
*jobs* - Will be run in parallel, within the confines of each stage.  
*scripts* - Will be run sequentially within the confines of each job.  

---

![image](gitlab_pipelines.png?)

### A Basic GitLab CI Pipeline

Save [`.gitlab-ci.yml`](files/.gitlab-ci.yml) in to your webserver project at `~/gitlab/mywebserver`

An analysis of the `.gitlab-ci.yml` example file; the important bits:

- We're telling GitLab to build our project inside the gitlab/dind (Docker in Docker) container.  Each job will run in its own container based on this image.

  ```yml
    image: gitlab/dind
  ```


- Stages define dependent steps in the workflow.  In some cases, you might have separate build, test, and deploy stages.  But since every job is run in its own container, this would complicate our example.  So we only have a single stage.

  ```yml
  stages:
    - build_test_push
  ```

- This is the meat of our pipeline.  We build the image, run it as a container, make sure it's serving a webpage, then push the image to the registry if everything is working.

  ```yml
  build_and_test_webserver:
    stage: build_test_push
    script:
      - docker build -t $CI_REGISTRY_IMAGE:$CI_BUILD_ID -t $CI_REGISTRY_IMAGE:latest .
      - docker run -d -p80:80 --name mywebserver $CI_REGISTRY_IMAGE:$CI_BUILD_ID
      - docker logs mywebserver
      - curl localhost:80
      - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
      - echo $CI_REGISTRY_IMAGE:latest
      - docker push $CI_REGISTRY_IMAGE
  ```

- The `docker logs mywebserver` line above is a very helpful command that will proactively add troubleshooting information to the logs (more on that in a second).

---

### Kicking it Off

- After you save your `.gitlab-ci.yml` file, you'll want to commit your changes to GitLab:

  ```bash
  cd ~/gitlab/mywebserver
  git add -A
  git commit -m 'add gitlab pipeline'
  git push
  ```
  ![image](gitlab_ci.png?)

  ![image](gitlab_ci2.png?)


  > aayore
    6:53 PM
    if you try to teach students that it's actually a **gitlab runner running docker with docker in docker building a docker image** ... you're going to extra-confuse people and run out of time.


---

### Checking Progress

- Go to [GitLab](https://gitlab.com) and navigate to your project
  - Should be: `https://gitlab.com/<your_user_name>/mywebserver`
- Click the `Pipelines` link
- If you don't see anything here, make sure you saved your `.gitlab-ci.yml` in the correct location, added, committed, and pushed your repo.
- You should see a `pending` or `running` (or maybe `passed` or `failed`) pipeline
- On the main Pipelines page, click on the status icon to see the pipeline details
- On the details page, click on the status icon to get information about the build (job) status
- You should see the output from your job on this page
  - This is where you can see the `docker logs ...` output
  - Take note of the output of the `$CI_REGISTRY_IMAGE:latest` command.
- Click the `Registry` link at the top of the page to view your image in the GitLab registry

---

## Checkpoint

#### This is a critical point.  If things don't look right at this point, review all the steps and ask for help.  You'll want everything working before you proceed to the next step.

---

### Other Tools/Resources

- [GitLab](https://docs.gitlab.com/ce/ci/yaml/README.html)  
- [CircleCI](https://circleci.com/docs/getting-started/)  
- [Docker-CI](http://docker-ci.org/documentation)  
- [Jenkins](https://jenkins.io/doc/)  
- [Travis CI](https://docs.travis-ci.com/user/getting-started/)  

---

_Continuous Integration_ is a software development practice where members of a team integrate their work frequently, usually each person integrates at least daily - leading to multiple integrations per day. Each integration is verified by an automated build (including test) to detect integration errors as quickly as possible. Many teams find that this approach leads to significantly reduced integration problems and allows a team to develop cohesive software more rapidly.

-- Martin Fowler

---

|Previous: [Cloud Infrastructure 2](/labs/05_cloud_infrastructure_2/cloud_infrastructure_2.md)|Next: [Manual Deployment](/labs/07_manual_deployment/manual_deployment.md)|
|----:|:----|
