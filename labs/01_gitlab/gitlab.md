# GitLab
*By the end of this lab, you will:* 
1. Setup SSH keys and use them to configure gitlab
1. Create and clone a new gitlab repo
1. Commit and push to your new gitlab repo
1. Clone an existing gitlab repo
1. Create a new git repo

---

### Get Started

- Set up [SSH keys](https://docs.gitlab.com/ce/gitlab-basics/create-your-ssh-keys.html)
   - Check to see if you have a public/private key pair in `~/.ssh/`
      - If so, you should see `id_rsa` and `id_rsa.pub` in that directory
      - If not, run `ssh-keygen` to generate a key pair
   - Log in to [GitLab](https://www.gitlab.com), navigate to the `SSH Keys` tab of your profile settings, and add the contents of your `id_rsa.pub` file as a new key.
- Find your [personal access token](https://gitlab.com/profile/personal_access_tokens) under your GitLab account settings
- Use the GitLab API to create a new repository

    ```bash
    TOKEN="<your_personal_access_token>"
    curl -X POST -H "PRIVATE-TOKEN: $TOKEN" https://gitlab.com/api/v4/projects?name=MyWebserver
    ```
![image](create_repo_gitlab.png?)

- Take note of the `ssh_url_to_repo` field's value in the output of that command
- Create a directory to house your work

    ```bash
    mkdir ~/gitlab
    cd ~/gitlab
    ```

- Clone the repository locally

    ```bash
    git clone <value_of_ssh_url_to_repo>
    cd mywebserver
    ```
![image](git_clone_repo_gitlab.png?)
---

### Working With Your Repo

- Create a new file

    ```bash
    echo "hello world" > gitlablab
    ```

- Check the status

    ```bash
    git status
    ```

- Track the file

    ```bash
    git add gitlablab
    ```

- Make a commit

    ```bash
    git commit -m 'add hello world'
    ```

- Make a change

    ```bash
    echo "this is a mistake" > gitlablab
    git add gitlablab
    git commit -m 'fix the file'
    ```

- You have everything locally, but you want to push it to GitLab so...
    - You have a backup
    - You can collaborate with others  

- Store all commits on GitLab

    ```bash
    git push
    ```

- But we messed up our file.  Let's fix it.

    ```bash
    echo "Hello World" > gitlablab
    git add gitlablab
    git commit -m 'actually fixed this time'
    git push
    ```

- Just for fun, let's delete (make sure your `git push` worked!) and recover our local copy of the file...

    ```bash
    ls -l
    rm gitlablab
    ls -l
    git checkout gitlablab
    ls -l
    ```

---

### Clone This Class Repository

```bash
cd ~/gitlab
git clone git@gitlab.com:sofreeus/introtodevops.git
```



---

### Create Your Own Project for This Class

```bash
cd ~/gitlab
mkdir devopsclass
cd devopsclass
git init
```

- The `git init` line tells git to start tracking this folder as a repository.  It doesn't matter that there's no remote GitLab repository.  You can still manage branches and commits locally.

---

### Further reading
  - [Basic git terminology](http://juristr.com/blog/2013/04/git-explained/#Terminology)
  - [More advanced terminology](https://cocoadiary.wordpress.com/2016/08/17/git-terminologyglossary/)
  - [Revert a commit](https://git-scm.com/docs/git-revert.html) (a.k.a. Undo; restoring from backup)
  - [Branching and merging](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)
  - [Git best practices](http://kentnguyen.com/development/visualized-git-practices-for-team/) (There are a lot of these.  Read critically and pick what works best in your environment.)
  - [Stashing](https://git-scm.com/book/en/v1/Git-Tools-Stashing)

---

|Previous: [Bash Warmup](/labs/00_bash_warmup/bash_warmup.md)|Next: [Docker](/labs/02_docker/docker.md)|
|---:|:---|
