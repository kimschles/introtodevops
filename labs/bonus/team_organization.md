# Team Organization

- What problem(s) are we trying to solve with DevOps?
- How does team organization impact the success of DevOps?
- T-Shaped People
- What does a product team look like?
- A collection of product teams?

---

### What problem(s) are we trying to solve with DevOps?
- Tension between development, operations, and product teams.
- Slow development cycles.
- Laborious deploys.
- Large deploys.
- Maintenance windows.

---

### How does team organization impact the success of DevOps?

Look at the traditional charters of the disciplines within your company.  Development wants change.  New features, new technologies, and ever more clever ways of doing things.  Operations wants stability.  Control.  As much mitigation of risk as possible.  A product team wants both of these things, but is rarely involved - beyond a superficial level - in the technical decision making.

Put differently, an Operations group that won't introduce development updates to production is a roadblock.  A Development group that breaks production with every change is a liability.  A Product team that promises feature delivery at an unsustainable rate can have a major detrimental impact on the company; both near term as a hit to morale and long term as extensibility was sacrificed for speed.  It is possible to work around these behaviors with large, brittle deploys, but that slows everything down.

Now imagine you have alignment across Development, Operations, and Product.  What is their common goal?  Product!  More specifically, delivering a stream of updates and improvements to production in a stable and sustainable way.  While it may be possible to accomplish this without changes to team structure, I've seen far more success among teams that are able to pull the dev, ops, and product functions into a single, accountable team.

---

### Skill sets: T-Shaped People vs. I-Shaped People

In many cases, you've got the people you've got.  In some cases you have the opportunity to build a team from scratch.  Either way, you need a model for skill sets that you should work toward.  I-shaped people have a very vertical skill set.

---

### A collection of product teams?

Not necessarily.  Product teams are great.  But your company may still have need for an operations group to maintain legacy apps.
