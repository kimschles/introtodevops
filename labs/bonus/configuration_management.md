# Configuration Management

When we talk about *configuration management* (CM), we're generally talking about using a tool or process to manage identical configurations across distributed systems.

Tools such as [Puppet](https://puppet.com/), [Chef](https://www.chef.io/), [Ansible](https://www.ansible.com/), or [Salt](https://saltstack.com/) are some of the most widely-used tools, but there are [many others](https://en.wikipedia.org/wiki/Comparison_of_open-source_configuration_management_software).  

---

### What Problem Does Configuration Management Solve?

Long-running systems can suffer a malady known as *configuration drift*.  An administrator may log in to a server to troubleshoot an issue.  New software updates may be installed.  Developers might update their code to a new version.  None of these things is inherently bad, but the aggregate effect is that we know much less about the system configuration than we did when it was freshly built.

A configuration management system will store a configuration centrally, and apply that change to the remote clients (which are usually servers).  Making changes in a configuration management system has several advantages over changing a server directly.

- You can track the changes in a version control system (such as git).
- You're less likely to have weird, one-off problems on a single server.
- It's much faster when a single change can be propagated to a group of machines automatically.
- You may find that some of your servers become stateless.
- A robust CM system can enable faster provisioning of servers.  Check out [Vagrant](https://www.vagrantup.com/)!

---

### How Does it Work?

Each CM tool is different.  Most of them enable administrators to group machines by type (web vs. database vs. app servers) and environment (dev/qa/uat/production) at a minimum.  Many allow you to define your own groupings, any apply multiple definitions to a single client.  For example, one client may get a `base` configuration as well as a `web server` configuration.

Some tools work by pushing a configuration to each client they know about.  Others run an agent on the client that checks in with a master server to pull down updates.

---

### Drawbacks

To quote from above:

> It's much faster when a single change can be propagated to a group of machines automatically.

That's a blessing and a curse.  When you're propagating a bugfix or a security patch, it's a very good thing.  When you're accidentally propagating a *bug*, it's a very bad thing.  The best way to mitigate this potential downfall is to roll changes through dev, qa, and uat, before they go to prod.

A CM system still requires discipline.  Maybe the pre-CM procedure required administrators to update the build documentation when they made a change to a running system.  Did that happen?  No.  Similarly, admins must exercise the discipline to make changes through the CM system, rather than going around it.

Long-running systems can still become problematic.  Clients that have undergone multiple iterations of configuration changes or version updates might exhibit unexpected symptoms.  If an administrator is able to leverage the CM system for new server builds, replacing older systems should be a much more attainable task.

---

### Immutable Infrastructure

Taking configuration management to an extreme can result in *immutable infrastructure*.  This is a paradigm in which the configuration of a running system is never modified.  A system should be replaced if any changes are needed, which means that bringing up a fully configured system needs to be fast and easy.

This sounds extreme, but it has a number of advantages:

- Scaling a service to meet demand is simple.
- Disaster recovery becomes easier.
- Configuration drift is minimal.

Purely immutable infrastructure is a difficult thing to attain.  The most pragmatic approach is a blend of CM and immutable infrastructure.  There is little downside to using some of each, and you can get all the advantages.

---

### Tool Selection

Configuration management tools are constantly evolving to add new capabilities.  Choosing the right one can be daunting, but take solace in the fact that they'll all get the job done.  The language used to document configurations is an important configuration; Puppet uses its own language, whereas Chef relies heavily on Ruby syntax.  If you have Ruby expertise in your environment, Chef might be a better choice than Puppet.  Evaluate the differences carefully, because it can be quite a commitment once you start using a CM tool.

#### Changeover Costs

On a related note, I have personally experienced a number of environments where the CM tool has been called in to question.  Generally not because of any flaw in the current tool, but rather because of an enticing feature in a different tool.  Depending how pervasive your use of CM has become, rewriting all your configs for a new system can be an incredibly time-consuming feat.  It will likely be faster to find a way to closely approximate other features with your own CM tool than to bring in a replacement CM tool.
