# Cloud Infrastructure
Part 1: Setting Up a Server in Amazon Web Services' (AWS) Elastic Compute Cloud (EC2)  

Recommended working directory: `~/gitlab/devopsclass/cloud_infrastructure`

---
### Objectives
By the end of this lab you will be able to:

- Describe what 'the cloud' is
- Describe a Region vs. an Availability Zone
- Deploy a compute instance in AWS
- Use a Cloud Formation Template (CFT)


### Verify Your AWS Access

- Log in to the [AWS console](https://console.aws.amazon.com/) at [https://console.aws.amazon.com/](https://console.aws.amazon.com/)
- Navigate to EC2 (under the Services menu at the top of the console)
- Make sure you are operating in the N. Virginia (us-east-1) or Oregon (us-west-2) region.

---

### Create an SSH Key

- From the left side of the EC2 page, select "Key Pairs"
- Click the "Create Key Pair" button
- Give the key pair a name (e.g. my_key)
- If prompted, save the private key to ~/.ssh/
- If not prompted, you may have to move the key

  ```bash
  mv ~/Downloads/my_key.pem ~/.ssh/
  ```

- Either way, you'll need to set the permissions on the private key file:

  ```bash
  chmod 0600 ~/.ssh/my_key.pem
  ```

---

### Allocate an Elastic IP

We need this so our IP won't change for the duration of the labs
- Go to the AWS EC2 console and click `Elastic IPs` on the left-side menu
- Click `Allocate New Address`
- Take note of the Allocation ID

---

### Define the Infrastructure

- Find a [demo_stack.yml](files/demo_stack.yml) file in 04_cloud_infrastructure_1/files
  - Save this file at `~/gitlab/devopsclass/cloud_infrastructure/demo_stack.yml`
  - Update the `KeyName` for `MyServer` to match the key you created above
  - Update the `AllocationID` for `MyElasticIP` to match the one you created above
  - Update the words after "echo" in the UserData Section 
 

---

### Build the Infrastructure

- Navigate to the CloudFormation console
  - Under the Services menu at the top of the console
- Click the "Create Stack" button
- Select the "Upload a template to Amazon S3" option
- Upload the `demo_stack.yml` file (in 'files' sub-folder)
- Give the stack a name
- Click through the rest of the GUI, picking the defaults

![image](aws.png?)

---

### Check Your Server

- Go to the AWS EC2 console and find the public IP of your instance
  - This should be the elastic IP that you created above
- Log in to the server and verify the Docker install

  ```bash
  ssh -i ~/.ssh/my_key.pem ubuntu@<server_public_ip>
  ```

- Are you in?  If so, you made a server!
- Check the build logs, then see if Docker is installed (it shouldn't be yet)

  ```bash
  cat /var/log/cloud-init-output.log  # Look for your echo command!
  docker --version
  ```

---

### Things to Note
- This server is not very secure
- No automated resiliency or redundancy
- We didn't install any configuration management

---

|Previous: [WebServer](/labs/03_webserver/webserver.md)|Next: [Cloud Infrastructure 2](/labs/05_cloud_infrastructure_2/cloud_infrastructure_2.md)|
|----:|:----|
