# Manual Deployment
*By the end of this lab, you will:*
1. Authenticate the server to the gitlab container registry
1. Add authentication in the CloudFormation template
1. Run the webserver on your EC2 instance
1. Confirm your webserver is running on the internet

Recommended working directory: `~/gitlab/devopsclass/cloud_infrastructure`
---

### Authenticate the Server to the Registry

- We need to put credentials on the server so it can access the Docker registry at GitLab:

  ```bash
  ssh -i ~/.ssh/my_key.pem ubuntu@<instance_public_ip>
  docker login registry.gitlab.com
  ```

- This will create a file at `~/.docker/config.json` on your server.
  - review it: `cat ~/.docker/config.json`

  ```json
  {
  	"auths": {
  		"registry.gitlab.com": {
  			"auth": "THIS-IS-YOUR-SECRET-KEY"
  		}
  	}
  }
  ```
![image](manual_deploy_prep.png?)

- Collapse it with this:

  ```shell
  cat .docker/config.json  | tr -d '[[:space:]]'
  ```

- to get this:

  ```json
  {"auths":{"registry.gitlab.com":{"auth":"THIS-IS-YOUR-SECRET-KEY"}}}
  ```

- Make sure you grab the contents of your own file, because `THIS-IS-YOUR-SECRET-KEY` is not actually a valid key.

---

### Authentication in the CloudFormation Template

- Let's add the above file to our CloudFormation template:

  ```yaml
  UserData:
    "Fn::Base64":
      "Fn::Join":
        - "\n"
        - - "#!/bin/bash"
          - apt update
          - apt install -y docker.io
          - echo '{"auths":{"registry.gitlab.com":{"auth":"THIS-IS-YOUR-SECRET-KEY"}}}' > /etc/docker/config.json
          - reboot
  ```

- Delete your CloudFormation stack
- Create a new CloudFormation stack using the updated template

---

### Run the Webserver on the Server

- Go to the AWS EC2 console and find the public IP of your instance
- Log in to the server and try to run your container, using the container you pushed to GitLab

  ```bash
  ssh -i ~/.ssh/my_key.pem ubuntu@<instance_public_ip>
  sudo docker --config /etc/docker run -d -p80:80 --name mywebserver registry.gitlab.com/<your_user_name>/mywebserver:latest
  sudo docker logs mywebserver
  ```

- If everything looks good, see if you can access your webpage
- From the server:

  ```bash
  curl localhost:80
  ```

![image](manual_deploy.png?)

- From your laptop, try to access the server's public IP in a web browser (and/or using the same curl command)

---

|Previous: [Continuous Integration](/labs/06_continuous_integration/continuous_integration.md)|Next: [Automated Deployment](/labs/08_automated_deployment/automated_deployment.md)|
|----:|:----|
