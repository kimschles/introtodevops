#!/bin/bash

GLORG=<your_user_name>
SERVER=<your_server_ip_address>

echo "Writing the key to /tmp/my_key.pem..."
echo "$SSH_KEY" > /tmp/my_key.pem

echo "Restrict permission on the private key file..."
chmod 0600 /tmp/my_key.pem

echo "Stop the old webserver..."
ssh -i /tmp/my_key.pem -o StrictHostKeyChecking=no ubuntu@$SERVER "sudo docker stop webserver"

echo "Remove the old webserver container..."
ssh -i /tmp/my_key.pem -o StrictHostKeyChecking=no ubuntu@$SERVER "sudo docker rm webserver"

echo "Update the webserver image..."
ssh -i /tmp/my_key.pem -o StrictHostKeyChecking=no ubuntu@$SERVER "sudo docker --config /etc/docker pull registry.gitlab.com/$GLORG/mywebserver:latest"

echo "Start the updated webserver..."
ssh -i /tmp/my_key.pem -o StrictHostKeyChecking=no ubuntu@$SERVER "sudo docker --config /etc/docker run -d -p80:80 --name webserver registry.gitlab.com/$GLORG/mywebserver:latest"

echo "Deploy complete (if questionably successful)"
