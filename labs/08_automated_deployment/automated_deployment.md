# Automated Deployment
*By the end of this lab, you will:*
1. Automate the steps to deploy your webserver using a bash script
1. Add a stage to the gitlab pipeline to automate the deployment the webserver 

Required working directory: `~/gitlab/mywebserver`  

---
### Update the Web Server

- Log in to your server.
- On the server, you can update the webserver with the newest container by stopping/removing the current container, then pulling and starting the new one:

  ```bash
  sudo docker stop mywebserver
  sudo docker rm mywebserver
  sudo docker --config /etc/docker pull registry.gitlab.com/your_user_name/mywebserver:latest
  sudo docker --config /etc/docker run -d -p80:80 --name mywebserver registry.gitlab.com/your_user_name/mywebserver:latest
  ```

- But we don't want to have to log on the server every time there's a new mywebserver version.
- Log out of the server.
- Run those same commands over SSH:

  ```bash
  ssh -i ~/.ssh/my_key.pem -o StrictHostKeyChecking=no ubuntu@<server_elastic_ip> 'sudo docker stop mywebserver'
  ssh -i ~/.ssh/my_key.pem -o StrictHostKeyChecking=no ubuntu@<server_elastic_ip> 'sudo docker rm mywebserver'
  ssh -i ~/.ssh/my_key.pem -o StrictHostKeyChecking=no ubuntu@<server_elastic_ip> 'sudo docker --config /etc/docker pull registry.gitlab.com/your_user_name/mywebserver:latest'
  ssh -i ~/.ssh/my_key.pem -o StrictHostKeyChecking=no ubuntu@<server_elastic_ip> 'sudo docker --config /etc/docker run -d -p80:80 --name mywebserver registry.gitlab.com/your_user_name/mywebserver:latest'
  ```

---

### Update With a Script

- Find `deploy.sh` file containing the SSH commands from above (in `08_automated_deployment/files`).
- Copy deploy.sh into your working directory (mywebserver)
- Fill in variables at top of the file with your Gitlab username and your EC2 IP address
- Make the `deploy.sh` file executable: `chmod +x deploy.sh`
- Run your `deploy.sh` script locally.  `./deploy.sh` Does it work?

---

### Deploy the Webserver with GitLab CI

To do this from GitLab, it'll need your private ssh key

- In the GitLab UI, go to your 'MyWebServer' Project, click on Settings --> CI/CD menu (on the left side), find the Variables section and click 'Expand'
  - Only the "Master" members of a GitLab project can see these variables.
- Add a variable called SSH_KEY
- Copy and paste your private key in as the variable value

  ```bash
  # To get the contents of your private key:
  cat ~/.ssh/my_key.pem
  ```

  ![image](automated_deploy.png?)

---

- Add a stage called `deploy` to your `.gitlab-ci.yml`
- Add a job called `update_mywebserver` (Or find the pre-made version in `08_automated_deployment/files` and copy to your working directory `mywebserver`)

  ```yaml
  update_webserver:
    stage: deploy
    script:
      - ./deploy.sh
  ```

---

|Previous: [Manual Deployment](/labs/07_manual_deployment/manual_deployment.md)|Next: [Everything All At Once](/labs/09_everything/whole_shebang.md)|
|----:|:----|
