# Intro to DevOps

---

### Class Goal

DevOps is an exceptionally broad discipline.  It requires development expertise, operations sensibility, an immense capacity for troubleshooting, eagerness to learn, and - maybe most importantly - a broad base of knowledge around tools that can be stitched together to accomplish the goal.  

This class will provide you with a very basic framework for workflow automation.  Every commit to a repo will be built, tested, and deployed.

There is no Holy Grail - every environment is different.  This course attempts to use mostly free, open-source software to show that workflow automation is a real thing that can be done.  We'll cover one way to do it, but there are many, many other ways to accomplish the same thing.  (I've also done a GitHub --> CircleCI --> AWS/Mesos/Marathon setup that does something very similar to what we'll see in this course.)

---

### Success Metric

Measure - or estimate - the time it takes for a code commit to be deployed in your production environment.  This is the main metric we'll use to measure the success of the DevOps workflow we build in this class.

---

### Prerequisites:

- Before starting this class, you should have a basic familiarity with a Unix-like shell (sh, bash, zsh, ksh, etc.)
- Laptop
  - Something with a Bash-like shell is preferred (Mac or Linux)
  - Windows machines may be a challenge (Maybe use [VirtualBox](https://www.virtualbox.org/) to run a Linux VM?)
  - Make sure you have [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) installed
- [Create a GitLab account](https://gitlab.com/users/sign_in)
  - If you already have one, you can disregard this
  - If you create a new one, you might consider making it something that you could take from company to company
- [Install Docker on your laptop](https://hub.docker.com/search/?type=edition&offering=community)
- [Create a free-tier AWS account](https://aws.amazon.com/)

***

### Following Instructions

Please follow the documented and stated instructions as closely as possible.  This will help mitigate issues that arise due to funky configurations.  As mentioned above, we'll be stitching together a number of tools.  This means that the labs are very inter-dependent, and an innocent deviation in an early lab could complicate a later lab.

I encourage all students to experiment and explore the material.  Making it your own and having fun with it will probably increase the functional utility of this class immensely.  I'm happy to help with any extracurricular questions and/or interests related to the material, but please try the extra stuff after completing the suggested stuff.  And maybe in a different subdirectory.  :)

***

# Labs

### _Fundamentals_

### 0. [Bash Warmup](labs/00_bash_warmup/bash_warmup.md)

### 1. [GitLab](labs/01_gitlab/gitlab.md)

### 2. [Docker](labs/02_docker/docker.md)

### 3. [Basic Webserver](labs/03_webserver/webserver.md)

### 4. [Cloud Infrastructure 1](labs/04_cloud_infrastructure_1/cloud_infrastructure_1.md)

### _The Good Stuff_

### 5. [Cloud Infrastructure 2](labs/05_cloud_infrastructure_2/cloud_infrastructure_2.md)

### 6. [Continuous Integration](labs/06_continuous_integration/continuous_integration.md)

### 7. [Manual Deployment](labs/07_manual_deployment/manual_deployment.md)

### 8. [Automated Deployment](labs/08_automated_deployment/automated_deployment.md)

### 9. [Everything All At Once](labs/09_everything/whole_shebang.md) (a.k.a. The Easy Lab)

***

### Bonus Labs

### + [Configuration Management](/labs/bonus/configuration_management.md)

### + [Team Organization](labs/bonus/team_organization.md)

---

### Recommended Reading
- [The Phoenix Project](https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business/dp/0988262509/), by Gene Kim
  - This is a rewrite of *The Goal* for the modern age.
- [The Goal](https://www.amazon.com/Goal-Process-Ongoing-Improvement/dp/0884271951/), by Eliyahu Goldratt
  - This is the original, and I think you'll benefit from doing the modern adaptation yourself.

---

### #FIXME: Things This Course Does Poorly
- Don't use the [root account](http://docs.aws.amazon.com/general/latest/gr/root-vs-iam.html) with AWS.
- Use [public and private subnets](http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Scenario2.html) with your VPC.
- Set up a [VPN](http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/vpn-connections.html) to your VPC.
- Don't store your Docker credentials in your CloudFormation template.  Use an [IAM role for your instance](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/UsingIAM.html), store the credentials in a secure S3 bucket, and use the [AWS CLI tools](http://docs.aws.amazon.com/cli/latest/reference/s3/index.html) on the instance to pull down the credentials file.
- Putting your private key in your GitLab project as a variable is a *really* bad practice.
